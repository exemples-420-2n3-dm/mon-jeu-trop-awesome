# Contribuer

## T&eacute;l&eacute;charger la derni&ecirc;re version du projet

```bash
$ git clone https://gitlab.com/exemples-420-2n3-dm/mon-jeu-trop-awesome.git
```

## Compiler

```bash
$ npm install docsify-cli -g
$ cd public
$ docsify serve
```

## Diagramme de classes

![Diagramme de classes](_media/diagramme-de-classes.png)
