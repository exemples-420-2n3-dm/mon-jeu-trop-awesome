# T&eacute;l&eacute;chargement

## Binaire Windows

- <a href="_media/binaire-windows.txt" download>binaire-windows.txt</a>

## Binaire Linux

- ...

## Code Source

- [Format zip](https://gitlab.com/exemples-420-2n3-dm/mon-jeu-trop-awesome/-/archive/master/mon-jeu-trop-awesome-master.zip)
- [Format tar.gz](https://gitlab.com/exemples-420-2n3-dm/mon-jeu-trop-awesome/-/archive/master/mon-jeu-trop-awesome-master.tar.gz)
- [Format tar.bz2](https://gitlab.com/exemples-420-2n3-dm/mon-jeu-trop-awesome/-/archive/master/mon-jeu-trop-awesome-master.tar.bz2)
- [Format tar](https://gitlab.com/exemples-420-2n3-dm/mon-jeu-trop-awesome/-/archive/master/mon-jeu-trop-awesome-master.tar)
